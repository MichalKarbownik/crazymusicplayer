package com.example.crazymusicplayer.player

import android.content.*
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import com.example.crazymusicplayer.data.Song
import com.example.crazymusicplayer.R
import com.example.crazymusicplayer.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_player.*
import java.util.*

class PlayerFragment : Fragment() {
    private lateinit var viewPager: ViewPager
    private lateinit var songsData: ArrayList<Song>
    private lateinit var titleTextView: TextView
    private lateinit var albumCoverImageView: ImageView
    private lateinit var artistTextView: TextView
    private lateinit var progressSeekBar: SeekBar
    private lateinit var previousButton: ImageButton
    private lateinit var playButton: ImageButton
    private lateinit var nextButton: ImageButton
    private lateinit var playlistButton: ImageButton
    private val durationHandler = Handler()

    private lateinit var playerService: PlayerService
    private lateinit var playerIntent: Intent
    private var isBound = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initService()
        this.songsData = arguments?.getParcelableArrayList(SONG_DATA_KEY) ?: ArrayList()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.context!!.unbindService(playerConnection)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUIElements()
        registerListeners()
    }

    private fun initService() {
        if (!isBound) {
            this.playerIntent = Intent(activity, PlayerService::class.java)
            isBound = true
            this.context!!.bindService(playerIntent, playerConnection, Context.BIND_AUTO_CREATE)
        }
    }

    private fun initUIElements() {
        this.albumCoverImageView = cmp_player_image_album_cover
        this.titleTextView = cmp_player_text_song_title
        this.artistTextView = cmp_player_text_song_artist
        this.progressSeekBar = cmp_player_widget_seekbar
        this.previousButton = cmp_player_button_previous
        this.playButton = cmp_player_button_play
        this.nextButton = cmp_player_button_next
        this.playlistButton = cmp_player_button_playlist

        this.viewPager = this.activity!!.cmp_main_view_pager
    }

    private fun registerListeners() {
        this.previousButton.setOnClickListener { onPreviousClick() }
        this.playButton.setOnClickListener { onPlayClick() }
        this.nextButton.setOnClickListener { onNextClick() }

        this.progressSeekBar.setOnSeekBarChangeListener(OnProgressSeekBarChangeListener())
        this.playlistButton.setOnClickListener { onPlaylistClick() }
    }

    private fun onPreviousClick() {
        playerService.playPreviousSong()
    }

    private fun onPlayClick() {
        playerService.togglePlaySong()
        togglePlayButton()
    }

    private fun onNextClick() {
        playerService.playNextSong()
    }

    private fun onPlaylistClick() {
        this.viewPager.currentItem = PLAYLIST_FRAGMENT_PAGE_NUMBER
    }

    fun changeSong(songIndex: Int) {
        playerService.playSong(songIndex)
        loadSong(songIndex)
        resumeSong()
    }

    fun pauseSong() {
        this.playerService.pauseSong()
        this.playButton.setImageResource(R.drawable.ic_play_circle_filled_white_96dp)
    }

    private fun resumeSong() {
        this.playerService.resumeSong()
        this.playButton.setImageResource(R.drawable.ic_pause_circle_filled_white_96dp)
    }

    private fun loadSong(songIndex: Int) {
        setSongLayout(songIndex)
        startUpdatingProgressBar()
    }

    private fun setSongLayout(songIndex: Int) {
        this.titleTextView.text = this.songsData[songIndex].title
        this.artistTextView.text = this.songsData[songIndex].artist
        setAlbumCover(songIndex)
    }

    private fun setAlbumCover(songIndex: Int) {
        val albumId = this.songsData[songIndex].albumId
        PlayerUtils.setAlbumCover(albumId, this.albumCoverImageView, R.drawable.ic_default_album_cover)
    }

    private fun togglePlayButton() {
        if(this.playerService.isPlaying()) {
            this.playButton.setImageResource(R.drawable.ic_pause_circle_filled_white_96dp)
        }
        else {
            this.playButton.setImageResource(R.drawable.ic_play_circle_filled_white_96dp)
        }
    }

    private inner class OnSongChangedListener: PlayerService.OnSongChangedListener {
        override fun onSongChanged() {
            loadSong(playerService.currentSongIndex)
        }
    }

    private inner class OnProgressSeekBarChangeListener : SeekBar.OnSeekBarChangeListener {

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            //not used but required to override
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {
            durationHandler.removeCallbacks(updateProgressBar)
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            durationHandler.removeCallbacks(updateProgressBar)
            val totalDuration = playerService.getSongDuration()
            val currentPosition = PlayerUtils.progressToTime(seekBar!!.progress, totalDuration)

            playerService.seekTo(currentPosition)
            startUpdatingProgressBar()
        }
    }

    private fun startUpdatingProgressBar() {
        this.progressSeekBar.progress = 0
        this.progressSeekBar.max = 100

        durationHandler.postDelayed(updateProgressBar, 0)
    }

    private val updateProgressBar = object : Runnable {
        override fun run() {
            durationHandler.removeCallbacks(this)
            val duration = playerService.getSongDuration().toLong()
            val currentDuration = playerService.getCurrentPosition().toLong()

            progressSeekBar.progress = PlayerUtils.percentageOfTime(currentDuration, duration)
            durationHandler.postDelayed(this, UPDATE_PROGRESS_BAR_INTERVAL)
        }
    }

    private val playerConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as PlayerService.MusicBinder
            playerService = binder.service
            isBound = true

            playerService.setOnSongChangedListener(OnSongChangedListener())
            loadSong(playerService.currentSongIndex)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            isBound = false
        }
    }
}