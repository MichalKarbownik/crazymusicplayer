package com.example.crazymusicplayer.sensors

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

class ShakeDetector : SensorEventListener {
    private val SHAKE_THRESHOLD_GRAVITY = 2.7f
    private val SHAKE_SLOP_TIME_MS = 500
    private val SHAKE_COUNT_RESET_TIME_MS = 2000
    private val MIN_NUMBER_OF_SHAKES = 2

    private lateinit var shakeListener: OnShakeListener
    private var shakeStartTime: Long = 0
    private var numberOfShakes: Int = 0

    interface OnShakeListener {
        fun onShake()
    }

    fun setOnShakeListener(listener: OnShakeListener) {
        this.shakeListener = listener
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        //not used but required to override
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            val xAxis = event.values[0]
            val yAxis = event.values[1]
            val zAxis = event.values[2]

            val gravityX = xAxis / SensorManager.GRAVITY_EARTH
            val gravityY = yAxis / SensorManager.GRAVITY_EARTH
            val gravityZ = zAxis / SensorManager.GRAVITY_EARTH

            val gForce =
                Math.sqrt((gravityX * gravityX + gravityY * gravityY + gravityZ * gravityZ).toDouble()).toFloat()

            if (gForce > SHAKE_THRESHOLD_GRAVITY) {
                val currentTime = System.currentTimeMillis()

                if (shakeStartTime + SHAKE_SLOP_TIME_MS > currentTime) {
                    return
                }


                if (shakeStartTime + SHAKE_COUNT_RESET_TIME_MS < currentTime) {
                    numberOfShakes = 0
                }

                shakeStartTime = currentTime
                numberOfShakes++

                if (numberOfShakes >= MIN_NUMBER_OF_SHAKES) {
                    shakeListener.onShake()
                }
            }
        }
    }
}