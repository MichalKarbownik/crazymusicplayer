package com.example.crazymusicplayer.utils

import android.content.ContentUris
import android.net.Uri
import android.widget.ImageView


class PlayerUtils {
    companion object {
        fun percentageOfTime(currentTime: Long, totalTime: Long): Int {
            val currentSeconds = (currentTime / 1000).toInt()
            val totalSeconds = (totalTime / 1000).toInt()

            val percentage = ((currentSeconds).toDouble() / totalSeconds) * 100

            return percentage.toInt()
        }

        fun progressToTime(progress: Int, totalDuration: Int): Int {
            val currentDuration = (progress.toDouble() / 100 * totalDuration / 1000).toInt()

            return currentDuration * 1000
        }

        fun setAlbumCover(albumId: Long, albumCoverImageView: ImageView, placeholder: Int) {
            val albumUri = getAlbumURI(albumId)
            albumCoverImageView.setImageURI(albumUri)

            if(albumCoverImageView.drawable == null) {
                albumCoverImageView.setImageResource(placeholder)
            }
        }

        private fun getAlbumURI(albumId: Long): Uri {
            return ContentUris.withAppendedId(Uri.parse(UTILS_ALBUMS_URI), albumId)
        }

    }
}