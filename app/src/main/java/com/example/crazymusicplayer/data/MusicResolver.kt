package com.example.crazymusicplayer.data

import android.content.ContentResolver
import android.content.Context
import android.provider.MediaStore

class MusicResolver(context: Context?) : ContentResolver(context) {
    val listOfSongs: ArrayList<Song> = ArrayList()

    init {
        val projectionColumns: Array<String> = arrayOf("IS_MUSIC", "_ID", "TITLE", "ARTIST", "DURATION", "ALBUM_ID")
        val musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val cursor = context!!.contentResolver.query(musicUri, projectionColumns, null, null, null)

        if (cursor != null && cursor.moveToFirst()) {
            val isMusic = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.IS_MUSIC)
            val idColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID)
            val titleColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE)
            val artistColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ARTIST)
            val durationColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.DURATION)
            val albumIdColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ALBUM_ID)

            do {
                if (cursor.getInt(isMusic) > 0) {
                    val songId = cursor.getLong(idColumn)
                    val songTitle = cursor.getString(titleColumn)
                    val songArtist = cursor.getString(artistColumn)
                    val songDuration = cursor.getString(durationColumn)
                    val albumId = cursor.getLong(albumIdColumn)
                    listOfSongs.add(Song(songId, songTitle, songArtist, songDuration, albumId))
                }
            } while (cursor.moveToNext())

            cursor.close()
        }
    }
}