package com.example.crazymusicplayer.utils

const val PLAYER_SERVICE_SHARED_PREFERENCES = "cmp_player_service_shared_preferences"
const val PLAYER_SERVICE_CURRENT_SONG_INDEX_KEY = "cmp_player_service_current_song_index_key"
const val SONG_DATA_KEY = "cmp_song_data_key"

const val UPDATE_PROGRESS_BAR_INTERVAL: Long = 100

const val PLAYER_FRAGMENT_PAGE_NUMBER = 0
const val PLAYLIST_FRAGMENT_PAGE_NUMBER = 1

const val UTILS_ALBUMS_URI = "content://media/external/audio/albumart"