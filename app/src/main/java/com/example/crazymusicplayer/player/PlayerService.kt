package com.example.crazymusicplayer.player

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.os.PowerManager
import com.example.crazymusicplayer.data.Song
import com.example.crazymusicplayer.utils.PLAYER_SERVICE_CURRENT_SONG_INDEX_KEY
import com.example.crazymusicplayer.utils.PLAYER_SERVICE_SHARED_PREFERENCES
import java.util.ArrayList

class PlayerService : Service(), MediaPlayer.OnCompletionListener {

    private lateinit var sharedPreferences: SharedPreferences
    private val musicPlayer = MediaPlayer()

    lateinit var songsData: ArrayList<Song>
    var currentSongIndex = 0
        private set

    private val playerBind = MusicBinder()
    private var isPlaying = false

    private lateinit var songChangedListener: OnSongChangedListener

    interface OnSongChangedListener {
        fun onSongChanged()
    }

    fun setOnSongChangedListener(listener: OnSongChangedListener) {
        this.songChangedListener = listener
    }

    override fun onCreate() {
        super.onCreate()
        this.sharedPreferences =
            this.applicationContext.getSharedPreferences(PLAYER_SERVICE_SHARED_PREFERENCES, Context.MODE_PRIVATE)
        this.currentSongIndex = this.sharedPreferences.getInt(PLAYER_SERVICE_CURRENT_SONG_INDEX_KEY, 0)

        initMusicPlayer()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.sharedPreferences.edit().putInt(PLAYER_SERVICE_CURRENT_SONG_INDEX_KEY, currentSongIndex).apply()
    }

    private fun initMusicPlayer() {
        musicPlayer.setWakeMode(this.applicationContext,
            PowerManager.PARTIAL_WAKE_LOCK)
        musicPlayer.setOnCompletionListener(this)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return playerBind
    }

    override fun onUnbind(intent: Intent): Boolean {
        musicPlayer.stop()
        musicPlayer.release()
        return false
    }

    override fun onCompletion(mp: MediaPlayer?) {
        if(isPlaying) {
            playNextSong()
        }
        else {
            prepareSong()
        }
    }

    fun playSong(songIndex: Int) {
        currentSongIndex = songIndex
        playSong()
    }

    fun playNextSong() {
        currentSongIndex = incrementSongIndex(currentSongIndex)
        songChangedListener.onSongChanged()
        playSong()
    }

    fun playPreviousSong() {
        currentSongIndex = decrementSongIndex(currentSongIndex)
        songChangedListener.onSongChanged()
        playSong()
    }

    fun playRandomSong() {
        val randomIndex = (0..this.songsData.lastIndex).random()
        this.currentSongIndex = randomIndex
        songChangedListener.onSongChanged()
        playSong()
    }

    fun togglePlaySong() {
        if (!this.musicPlayer.isPlaying) {
            resumeSong()
        } else {
            pauseSong()
        }
    }

    fun resumeSong() {
        this.isPlaying = true
        this.musicPlayer.start()
    }

    fun pauseSong() {
        this.isPlaying = false
        this.musicPlayer.pause()
    }

    fun getSongDuration(): Int {
        return this.musicPlayer.duration
    }

    fun seekTo(currentPosition: Int) {
        this.musicPlayer.seekTo(currentPosition)
    }

    fun getCurrentPosition(): Int {
        return this.musicPlayer.currentPosition
    }

    fun isPlaying(): Boolean {
        return this.musicPlayer.isPlaying
    }

    private fun playSong() {
        prepareSong()
        musicPlayer.start()

        if(!isPlaying) {
            musicPlayer.pause()
        }
    }

    private fun prepareSong() {
        val currentSongUri = songsData[currentSongIndex].getURI()
        musicPlayer.reset()
        musicPlayer.setDataSource(applicationContext, currentSongUri)
        musicPlayer.prepare()
    }

    private fun incrementSongIndex(songIndex: Int): Int {
        return if (songIndex == songsData.lastIndex)
            0
        else
            songIndex.inc()
    }

    private fun decrementSongIndex(songIndex: Int): Int {
        return if (songIndex == 0)
            songsData.lastIndex
        else
            songIndex.dec()
    }

    inner class MusicBinder : Binder() {
        internal val service: PlayerService
            get() = this@PlayerService
    }
}