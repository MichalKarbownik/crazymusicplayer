package com.example.crazymusicplayer

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import com.example.crazymusicplayer.data.MusicResolver
import com.example.crazymusicplayer.data.Song
import com.example.crazymusicplayer.player.PlayerFragment
import com.example.crazymusicplayer.player.PlayerService
import com.example.crazymusicplayer.playlist.PlaylistAdapter
import com.example.crazymusicplayer.playlist.PlaylistFragment
import com.example.crazymusicplayer.sensors.CoverDetector
import com.example.crazymusicplayer.sensors.ShakeDetector
import com.example.crazymusicplayer.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import com.example.crazymusicplayer.player.PlayerService.MusicBinder

class MainActivity : AppCompatActivity(), PlaylistAdapter.ViewHolderListener {
    private val NUMBER_OF_PAGES = 2
    private lateinit var viewPager: ViewPager
    private lateinit var playerFragment: PlayerFragment
    private lateinit var playlistFragment: PlaylistFragment
    private lateinit var musicResolver: MusicResolver
    private lateinit var songs: ArrayList<Song>

    private lateinit var sensorManager: SensorManager
    private lateinit var accelerometerSensor: Sensor
    private val shakeDetector: ShakeDetector = ShakeDetector()
    private lateinit var proximitySensor: Sensor
    private val coverDetector: CoverDetector = CoverDetector()

    private lateinit var playerService: PlayerService
    private lateinit var playerIntent: Intent
    private var isBound = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initMusicData()
        initFragments()
        initUserInterface()
        initSensors()
    }

    override fun onStart() {
        super.onStart()
        initService()
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(playerConnection)
    }

    override fun onBackPressed() {
        if (viewPager.currentItem != 0) {
            viewPager.currentItem -= 1
        }
        else {
            moveTaskToBack(true)
        }

    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(shakeDetector, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL)
        sensorManager.registerListener(coverDetector, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(shakeDetector)
        sensorManager.unregisterListener(coverDetector)
    }

    private fun initMusicData() {
        this.musicResolver = MusicResolver(this)
        this.songs = this.musicResolver.listOfSongs
    }

    private fun initService() {
        if (!isBound) {
            this.playerIntent = Intent(this, PlayerService::class.java)
            bindService(playerIntent, playerConnection, Context.BIND_AUTO_CREATE)
            startService(playerIntent)
        }
    }

    private fun initFragments() {
        val songsData = Bundle()
        songsData.putParcelableArrayList(SONG_DATA_KEY, this.songs)

        this.playerFragment = PlayerFragment()
        this.playerFragment.arguments = songsData
        this.playlistFragment = PlaylistFragment()
        this.playlistFragment.arguments = songsData
    }

    private fun initUserInterface() {
        this.viewPager = cmp_main_view_pager
        this.viewPager.adapter = ViewPagerAdapter(supportFragmentManager)
    }

    private fun initSensors() {
        this.sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        this.accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        this.proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY)

        this.shakeDetector.setOnShakeListener(OnShakePlayer())
        this.coverDetector.setOnCoverListener(OnCoverPlayer())
    }

    inner class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
        override fun getCount(): Int {
            return NUMBER_OF_PAGES
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                PLAYER_FRAGMENT_PAGE_NUMBER -> playerFragment
                PLAYLIST_FRAGMENT_PAGE_NUMBER -> playlistFragment
                else -> playerFragment
            }
        }
    }

    override fun onPlaylistItemClicked(layoutPosition: Int) {
        playerFragment.changeSong(layoutPosition)
    }

    inner class OnShakePlayer : ShakeDetector.OnShakeListener {
        override fun onShake() {
            playerService.playRandomSong()
        }
    }

    inner class OnCoverPlayer : CoverDetector.OnCoverListener {
        override fun onCover() {
            playerFragment.pauseSong()
        }
    }

    private val playerConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as MusicBinder
            playerService = binder.service
            playerService.songsData = songs
            isBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            isBound = false
        }
    }
}
