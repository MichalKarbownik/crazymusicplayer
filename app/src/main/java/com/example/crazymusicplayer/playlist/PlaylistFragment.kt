package com.example.crazymusicplayer.playlist

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.crazymusicplayer.data.Song
import com.example.crazymusicplayer.MainActivity
import com.example.crazymusicplayer.player.PlayerService
import com.example.crazymusicplayer.R
import com.example.crazymusicplayer.utils.SONG_DATA_KEY
import kotlinx.android.synthetic.main.fragment_playlist.*

class PlaylistFragment : Fragment() {
    private lateinit var songsData: ArrayList<Song>
    private lateinit var playlistRecyclerView: RecyclerView
    private lateinit var playlistAdapter: PlaylistAdapter

    private lateinit var playerService: PlayerService
    private lateinit var playerIntent: Intent
    private var isBound = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initService()
        this.songsData = arguments?.getParcelableArrayList(SONG_DATA_KEY) ?: ArrayList()
        val listener = activity as MainActivity
        this.playlistAdapter = PlaylistAdapter(this.songsData, listener)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_playlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.playlistRecyclerView = cmp_playlist_recycler_view

        this.playlistRecyclerView.adapter = this.playlistAdapter
        this.playlistRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    private fun initService() {
        if (!isBound) {
            this.playerIntent = Intent(activity, PlayerService::class.java)
            isBound = true
            this.context!!.bindService(playerIntent, playerConnection, Context.BIND_AUTO_CREATE)
        }
    }

    private val playerConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as PlayerService.MusicBinder
            playerService = binder.service
            isBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            isBound = false
        }
    }
}