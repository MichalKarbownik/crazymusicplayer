package com.example.crazymusicplayer.sensors

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener

class CoverDetector : SensorEventListener {
    private val COVER_THRESHOLD = 0.5f

    private lateinit var coverListener: OnCoverListener
    private var lastProximity = 1

    interface OnCoverListener {
        fun onCover()
    }

    fun setOnCoverListener(listener: OnCoverListener) {
        this.coverListener = listener
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        //not used but required to override
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (isProximitySensor(event.sensor.type) && proximityChanged(event.values[0])) {
            coverListener.onCover()
        }
    }

    private fun proximityChanged(currentProximity: Float): Boolean {
        return currentProximity <= COVER_THRESHOLD && this.lastProximity >= COVER_THRESHOLD
    }

    private fun isProximitySensor(sensorType: Int): Boolean {
        return sensorType == Sensor.TYPE_PROXIMITY
    }
}