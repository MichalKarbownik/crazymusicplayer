package com.example.crazymusicplayer.playlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.crazymusicplayer.data.Song
import com.example.crazymusicplayer.R
import com.example.crazymusicplayer.utils.PlayerUtils
import kotlinx.android.synthetic.main.fragment_playlist_song_item.view.*

class PlaylistAdapter(private val songsData: ArrayList<Song>, vHListener: ViewHolderListener) :
    RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder>() {
    private var viewHolderListener: ViewHolderListener = vHListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistAdapter.PlaylistViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_playlist_song_item, parent, false)

        return PlaylistViewHolder(view)
    }

    override fun getItemCount(): Int = songsData.size

    override fun onBindViewHolder(viewHolder: PlaylistViewHolder, position: Int) {
        val currentSong = songsData[position]
        viewHolder.titleTextView.text = currentSong.title
        viewHolder.artistTextView.text = currentSong.artist
        PlayerUtils.setAlbumCover(currentSong.albumId, viewHolder.albumCoverImageView, R.drawable.ic_default_album_cover)
    }

    inner class PlaylistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val titleTextView: TextView = itemView.cmp_playlist_text_song_title
        val artistTextView: TextView = itemView.cmp_playlist_text_song_artist
        val albumCoverImageView: ImageView = itemView.cmp_playlist_image_album_cover

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            viewHolderListener.onPlaylistItemClicked(layoutPosition)
            notifyItemChanged(layoutPosition)
        }
    }

    interface ViewHolderListener {
        fun onPlaylistItemClicked(layoutPosition: Int)
    }
}